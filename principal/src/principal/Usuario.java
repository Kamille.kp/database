package principal;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//@author kamil
 
public class Usuario {
    private String nome, usuario, email, senha;
    private String emailAntigo;
    
    //2º parte
    public static ArrayList<Usuario> getAll(){
        ArrayList lista = new ArrayList<Usuario>();
        return lista;
    }
    public void inserir(){}
    public void load(){}
    public void alterar(){
        Conexao c = new Conexao();
        Connection con = c.getConexao();
    
        String sql = "UPDATE usuário\nSET nome = ?, email = ?\nWHERE email = ?";///?????
        try{
            PreparedStatement ps = con.prepareStatement(sql);
            
            ps.setString(3, this.emailAntigo); //Observe o indice da chave primaria???
            
            ps.setString(1, this.nome);
            ps.setString(2, this.email);
            
            
            ps.executeUpdate();        
            
            this.emailAntigo = email;
            
        }
        catch(SQLException e){            
            System.out.println("deu caca");
            e.printStackTrace();
        }
    }
    public void deletar(){}
    
    //1º parte
    public Boolean validar(){
        String[] stringE = email.split(" "); 
        String[] stringS = senha.split(" "); 
        int i, cont=0; 
        boolean teste;
        
        for(i=0;i<email.length();i++){
            if("@".equals(stringE[i])){
                cont++;
            } 
        }
        for(i=0;i<6;i++){
            if(!senha.matches("[A-Z]*")){
                cont++;
            }
        }
        
        if(cont == 2){
            teste = true;
        }
        else{
            teste = false;
        }
        
        return teste;
    }
    public void insert() throws SQLException{
        Conexao c = new Conexao(); 
        String insertTableSQL;
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
                      
        try{
            insertTableSQL = "insert into USUÁRIO\n(usuario, nome, email, senha)\n\nVALUES \n(?, ?, ?, ?)";

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);

            preparedStatement.setString(1,this.usuario);
            preparedStatement.setString(2, this.nome);
            preparedStatement.setString(3, this.email);
            preparedStatement.setString(4, this.senha);

            preparedStatement.executeUpdate();
        }  
        catch(SQLException e){
                e.printStackTrace();
        }
    }
    
    //Setter e Getters
    Usuario(String nome, String usuario, String email, String senha) {
        this.nome = nome;
        this.usuario = usuario;
        this.email = this.emailAntigo = email;
        this.senha = senha;
        this.email = this.emailAntigo;
    }
    public String getNome() {
        return nome;
    }
    public String getSenha() {
        return senha;
    }
    public String getUsuario() {
        return usuario;
    }
    public String getEmail() {
        return email;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public void setEmail(String email) {
        this.email = email;
    } 
}
