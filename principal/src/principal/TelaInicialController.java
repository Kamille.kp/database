package principal;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import principal.Usuario;

// @author kamille

public class TelaInicialController implements Initializable {
    @FXML
    private TextField nomeCompleto, usuario, email, senha, reSenha;
    @FXML 
    private TableColumn emailCol, nomeCol, usuarioCol;
    @FXML
    private Label mensagemSistema;
    @FXML
    private TableView <Usuario> tabela;
    @FXML
    private Label label;
      
    private Usuario usuarioSelecionado;
    
    @FXML
    private void cadastro(ActionEvent event) throws SQLException{
        Usuario u = new Usuario(nomeCompleto.getText(), usuario.getText(), email.getText(), senha.getText());
        int tam = this.senha.getText().length();
        
        if(tam < 7 && senha.getText().equals(reSenha.getText())){
            if(u.validar() == true){
                u.insert();
                mensagemSistema.setText("Tudo certo!");
            }
            else{
                mensagemSistema.setText("Verifique se o email está correto.");
            }      
        }
    }  
    @FXML
    private void selecionar(javafx.scene.input.MouseEvent event) {
        usuarioSelecionado = tabela.getSelectionModel().getSelectedItem();//Pega quem está selecionado na tabela
        nomeCompleto.setText(usuarioSelecionado.getNome());
        email.setText(usuarioSelecionado.getEmail());
        usuario.setText(usuarioSelecionado.getEmail());
    }
    @FXML
    private void atualiza(ActionEvent event){
        usuarioSelecionado.setEmail(email.getText());
        usuarioSelecionado.setNome(nomeCompleto.getText());  
        usuarioSelecionado.setUsuario(usuario.getText());  
        usuarioSelecionado.alterar();
        tabela.refresh();//comando mágico
    }
     
    private void makeColumns() {
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome")); 
        emailCol.setCellValueFactory(new PropertyValueFactory<>("email"));
        usuarioCol.setCellValueFactory(new PropertyValueFactory<>("usuario"));
    }
    private void addItems(){
        for (Usuario u2 : Usuario.getAll()) {
            tabela.getItems().add(u2);
        }
    }
  
    @Override
    public void initialize(URL location, ResourceBundle resources) { 
        this.makeColumns();
        this.addItems();
    }
}